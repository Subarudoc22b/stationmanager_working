//
//  DayModel.swift
//  StationManager
//
//  Created by Shawn Coombs on 8/25/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

struct DayModel {
    var id: String!
    var title = Date()
   // var subtitle = ""
    var morningReportModels = [MorningReportModel]()
    
    
    init(title: Date, data: [MorningReportModel]?) {
        id = UUID().uuidString
        self.title = title
      //  self.subtitle = subtitle
        
        
        if let data = data {
            self.morningReportModels = data
        }
    }
}

extension DayModel: Comparable {
    static func < (lhs: DayModel, rhs: DayModel) -> Bool {
        return lhs.title < rhs.title
    }

    static func == (lhs: DayModel, rhs: DayModel) -> Bool {
        return lhs.id == rhs.id
    }
}

