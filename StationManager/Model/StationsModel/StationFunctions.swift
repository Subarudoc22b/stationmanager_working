//
//  StationFunctions.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/13/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit


class StationFunctions {
    
    static func createStation(at divisionIndex: Int, using stationModels: StationModel) {
        // Replace with real data store code
        
        Data.divisionModels[divisionIndex].stationModels.append(stationModels)
        
    
    
}

    static func updateStation(at divisionIndex: Int,  using stationModels: StationModel) {
        
        let divisionModel = Data.divisionModels[divisionIndex]
        let stationIndex = (divisionModel.stationModels.firstIndex(of: stationModels))!
        Data.divisionModels[divisionIndex].stationModels[stationIndex] = stationModels
        
    }
    static func readStation(completion: @escaping () -> ()) {
        // Replace with real data store code
        
        DispatchQueue.global(qos: .userInitiated).async {
            if Data.stationModels.count == 0 {
                
                Data.stationModels = DivisionData.createStationModelData(divisionTitle: "Northern Division")
                //                Data.stationModels = DivisionData.createmockStationModelData(divisionTitle: "Northern Division")
                //                Data.stationModels = DivisionData.createmockStationModelData(divisionTitle: "Southern Division")
            }
            
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    static func readStation(by divisionIndex: Int ,by id: UUID, completion: @escaping (StationModel?) -> ()) {
        //   Replace with real data store code
        DispatchQueue.global(qos: .userInitiated).async {
            
            let station = Data.divisionModels[divisionIndex].stationModels.first(where: {$0.id == id})
            
            DispatchQueue.main.async {
                completion(station)
                
            }
        }
        
    }
    
}
