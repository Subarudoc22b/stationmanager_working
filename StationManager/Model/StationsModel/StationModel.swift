//
//  StationModel.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/12/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class StationModel: NSObject, MKAnnotation {
    var id: UUID
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var dayModels = [DayModel]() {
       didSet {
            // Called when a new value is assigned to dayModels
            //            dayModels = dayModels.sorted(by: { (dayModel1, dayModel2) -> Bool in
            //                dayModel1.title < dayModel2.title
            //            })

            //            dayModels = dayModels.sorted(by: { $0.title < $1.title })

            dayModels = dayModels.sorted(by: <)
        }
    }

    init(title: String, coordinate: CLLocationCoordinate2D, dayModels: [DayModel]? = nil) {
        
        id = UUID()
        self.title = title
        self.coordinate = coordinate
        
        super .init()
        
        
        if let dayModels = dayModels {
            self.dayModels = dayModels
        }
        
//        var markerTintColor: UIColor  {
//          
//    }
}

}
