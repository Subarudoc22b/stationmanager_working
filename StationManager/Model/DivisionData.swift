//
//  DivisionData.swift
//  StationManager
//
//  Created by Shawn Coombs on 8/25/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit


class DivisionData {
    static func createDivisionModelData() -> [DivisionModel] {
        var Divisions = [DivisionModel]()
        Divisions.append(DivisionModel(title: "Bridgetown Division", stationModels: createStationModelData(divisionTitle: "Bridgetown Division")))
        Divisions.append(DivisionModel(title: "Northern Division", stationModels: createStationModelData(divisionTitle: "Northern Division")))
        Divisions.append(DivisionModel(title: "Southern Division", stationModels: createStationModelData(divisionTitle: "Southern Division")))
        return Divisions
        
        //  Divisions.append(DivisionModel(title: "Bridgetown Division", stationModels: createmockStationModelData(divisionTitle: "Bridgetown Division"),dayModels: createmockDayModelData()))
        
        
        
        
        
    }
    
    
        static func createStationModelData(divisionTitle: String) -> [StationModel] {
        var Stations = [StationModel]()

        
        
        switch divisionTitle {
        case "Bridgetown Division":
            Stations.append(StationModel(title: "Central Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.099431, longitude: -59.615930),dayModels: createmockDayModelData()))
            Stations.append(StationModel(title: "Bridgetown Port Police Post",coordinate: CLLocationCoordinate2D(latitude: 13.100838, longitude: -59.627446)))
            Stations.append(StationModel(title: "Black Rock Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.113916, longitude: -59.615589)))
            Stations.append(StationModel(title: "District A Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.111749, longitude: -59.603846)))
            Stations.append(StationModel(title: "Hastings Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.078104, longitude: -59.605604)))
            Stations.append(StationModel(title: "Worthings Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.069231, longitude: -59.576605)))
        case "Northern Division":
            Stations.append(StationModel(title: "Holetown Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.185572, longitude: -59.637807)))
            Stations.append(StationModel(title: "District D Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.156457, longitude: -59.594513)))
            Stations.append(StationModel(title: "District E Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.251714, longitude: -59.643472)))
            Stations.append(StationModel(title: "District F Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.192642, longitude: -59.543998)))
            Stations.append(StationModel(title: "Crab Hill Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.315967, longitude: -59.632240)))
        case "Southern Division":
            Stations.append(StationModel(title: "Oistins Police Station" ,coordinate: CLLocationCoordinate2D(latitude: 13.061667, longitude: -59.540178)))
            Stations.append(StationModel(title: "District B Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.108632, longitude: -59.540386)))
            Stations.append(StationModel(title: "District C Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.147670, longitude: -59.480184)))
            Stations.append(StationModel(title: "Glebe Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.130130, longitude: -59.566742)))
            Stations.append(StationModel(title: "Airport Police Station",coordinate: CLLocationCoordinate2D(latitude: 13.078782, longitude: -59.490268)))
        default:
            Stations.append(StationModel(title: "No Station Available",coordinate: CLLocationCoordinate2D(latitude: 13.1939, longitude: -59.5432)))

            
        }

        return Stations
    }

    static func createmockDayModelData() -> [DayModel] {
        var dayModels = [DayModel]()

        dayModels.append(DayModel(title: Date(), data: createMockMorningReportModelData(sectionsubTitle: "February 26")))
        dayModels.append(DayModel(title: Date().add(days: 1), data: createMockMorningReportModelData(sectionsubTitle: "February 27")))
        dayModels.append(DayModel(title: Date().add(days: 2), data: createMockMorningReportModelData(sectionsubTitle: "February 28")))
//        dayModels.append(DayModel(title: Date().add(days: 3), data: createMockMorningReportModelData(sectionsubTitle: "February 29")))
//        dayModels.append(DayModel(title: Date().add(days: 4), data: createMockMorningReportModelData(sectionsubTitle: "February 30")))


        return dayModels


    }
    
    static func createMockMorningReportModelData(sectionsubTitle: String) -> [MorningReportModel] {
        var models = [MorningReportModel]()
        
        switch sectionsubTitle {
        case "February 26":
            
            models.append(MorningReportModel(title: "Possession Of Firearm", subtitle: " Gamal Nicholia Haynes 35 yrs of 7th Avenue New Orleans St. Micheal was arrested and charged that he on the 18th day of February 2019 committed the captioned offence whilst at Perry Street, Bridgetown St. Michael.n/ On the mentioned date as a result of information received personnel from the Anti-Gun and Gang unit conducted an operation in the area. The accused was seen driving a white Toyota motorcar bearing the registration number Z-105 which was intercepted and a search of the accused person revealed one (1) Glock Pistol concealed in the waistband of his pants.",  latitude: 13.096604, longitude: -59.616396,  reportType: ReportType.Possession_Of_Firearm, classType: "Major", incidentNumber: "ACR 59/2019"))
            models.append(MorningReportModel(title: "Missing Person", subtitle: "Terrance Ramsay 67yrs a Guyanese National with Barbadian Citizenship residing at Cheapside, St. Michael, left the Bridgetown Fishing Complex situated at Princess Alice Highway, St. Michael on board the fishing Vessel SEA PASSION registration number X149 accompanied by two other persons on 2017-12-14 around 17:00Hrs to go fishing in Tobago and about 02:00Hrs on 2017-12-15 went missing about 45 miles off the coast of Barbados", latitude: 13.097020, longitude: -59.622876, reportType: ReportType.Missing_Person, classType: "Missing", incidentNumber: ""))
            models.append(MorningReportModel(title: "Missing Person", subtitle: "Andrew King 49yrs of Deacons Road St. Michael attended a cruise dubbed Nice For What on board the Jolly Roger on 2018-06-15. About 02:30Hrs on 2018-06-16 he fell overboard from the second floor of the same pleasure craft. Video footage showed that Andrew King fell overboard and was not pushed.",latitude: 13.106806, longitude: -59.625642, reportType: ReportType.Missing_Person, classType: "Missing", incidentNumber: ""))
            
        case "February 27":
            models.append(MorningReportModel(title: "Robbery", subtitle: "", latitude: 13.094810, longitude: -59.610598, reportType: ReportType.Robbery, classType: "Major", incidentNumber: "ACR 58/2019"))
            
        case "February 28":
            models.append(MorningReportModel(title: "Burglary", subtitle: "", latitude: 13.096017, longitude: -59.603656, reportType: ReportType.Burglary, classType: "Major", incidentNumber: "ACR 59/2019"))
            
            
            
        default: models.append(MorningReportModel(title: "", subtitle: "",latitude: 13.100838, longitude: -59.627446, reportType: ReportType.Burglary, classType: "Major", incidentNumber: "ACR 59/2019"))
        }
        
        return models
    }
    
    
}

