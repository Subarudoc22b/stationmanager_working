//
//  MapModel.swift
//  StationManager
//
//  Created by Shawn Coombs on 6/11/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit

class MapModel: NSObject, MKAnnotation {
    var id:  UUID
    var title:  String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        id = UUID()
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
    }
}

