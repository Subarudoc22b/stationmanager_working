//
//  MorningReportFunctions.swift
//  StationManager
//
//  Created by Shawn Coombs on 8/25/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import Foundation
import MapKit

class MorningReportFunctions {
    
    static func createMorningReport(at divisionIndex: Int, at stationIndex: Int, for dayIndex: Int, using morningReportModel: MorningReportModel) {
        // Replace with real data store code
        
    Data.divisionModels[divisionIndex].stationModels[stationIndex].dayModels[dayIndex].morningReportModels.append(morningReportModel)
        
    }
    
    
}

