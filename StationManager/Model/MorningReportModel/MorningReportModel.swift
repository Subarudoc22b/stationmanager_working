//
//  MorningReport.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/18/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class MorningReportModel: NSObject, MKAnnotation {
    
    var id: String!
    var title: String?
    let subtitle: String?
    var coordinate: CLLocationCoordinate2D
    var reportType: ReportType?
    var classType: String?
    var incidentNumber: String?
    
    
    init(title: String, subtitle: String, latitude:CLLocationDegrees,longitude:CLLocationDegrees, reportType: ReportType, classType: String, incidentNumber: String) {
        
        id = UUID().uuidString
        self.title = title
        self.subtitle = subtitle
        self.coordinate = CLLocationCoordinate2D(latitude: latitude,longitude: longitude)
        self.reportType = reportType
        self.classType = classType
        self.incidentNumber = incidentNumber
        
        
        
        super .init()
        
        
        
    }
    
    
    
    
    var markerTintColor: UIColor  {
        
        switch reportType {
        case .Burglary?:
            return .blue
        case .Kidnapping?:
            return .purple
        case .Wounding?:
            return .red
        case .Assault_Occasioning_Actual_Bodily_Harm?:
            return .cyan
        case .Theft_Of_Money?:
            return .green
        case .Possession_Of_Control_Drug_Cannabis?:
            return .green
        case .Obstructing_Officer_In_Execution_Of_Duty?:
            return .darkGray
        case .Theft?:
            return .lightGray
        case .Possession_Of_Firearm?:
            return .black
        case .Missing_Person?:
            return .brown
        case .Robbery?:
            return .black
        default:
            return .darkText
            
            
            
        }
        
    }
    
    
    
    var imageName: UIImage? {
        
        switch reportType {
            
            
        case .Assault_Occasioning_Actual_Bodily_Harm:
               return #imageLiteral(resourceName: "Assault_Occasioning_Actual_Bodily_Harm")
           case .Burglary:
               return #imageLiteral(resourceName: "Burglary")
           case .Kidnapping:
               return #imageLiteral(resourceName: "Kidnapping")
           case .Missing_Person:
               return #imageLiteral(resourceName: "Missing_Person")
           case .Obstructing_Officer_In_Execution_Of_Duty:
               return #imageLiteral(resourceName: "Obstructing_Officer_In_Execution_Of_Duty")
           case .Possession_Of_Control_Drug_Cannabis:
               return #imageLiteral(resourceName: "Possession_Of_Control_Drug_Cannabis")
           case .Possession_Of_Firearm:
               return #imageLiteral(resourceName: "Possession_Of_Firearm")
           case .Robbery:
               return #imageLiteral(resourceName: "Robbery")
           case .Stabbing:
               return #imageLiteral(resourceName: "Stabbing")
           case .Theft_Of_Money:
               return #imageLiteral(resourceName: "Theft_Of_Money")
           case .Theft:
               return #imageLiteral(resourceName: "Theft")
           case .Wounding:
               return #imageLiteral(resourceName: "Wounding")
            
        default:
            return #imageLiteral(resourceName: "warning")
            
            
        }
        
        
    }
    
}
