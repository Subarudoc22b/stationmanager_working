//
//  TitleEnum.swift
//  StationManager
//
//  Created by Shawn Coombs on 7/16/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import Foundation

enum reportTitle: String {
    case Assault_Occasioning_Actual_Bodily_Harm
    case Burglary
    case Kidnapping
    case Missing_Person
    case Obstructing_Officer_In_Execution_Of_Duty
    case Possession_Of_Control_Drug_Cannabis
    case Possession_Of_Firearm
    case Robbery
    case Stabbing
    case Theft_Of_Money
    case Theft
    case Wounding
}

let pickerDataArray = [
    reportTitle.Assault_Occasioning_Actual_Bodily_Harm.rawValue,
    reportTitle.Burglary.rawValue,
    reportTitle.Kidnapping.rawValue,
    reportTitle.Missing_Person.rawValue,
    reportTitle.Obstructing_Officer_In_Execution_Of_Duty.rawValue,
    reportTitle.Possession_Of_Control_Drug_Cannabis.rawValue,
    reportTitle.Possession_Of_Firearm.rawValue,
    reportTitle.Robbery.rawValue,
    reportTitle.Stabbing.rawValue,
    reportTitle.Theft_Of_Money.rawValue,
    reportTitle.Theft.rawValue,
    reportTitle.Wounding.rawValue
]
