//
//  ReportTypeEnum.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/18/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import Foundation

enum ReportType: Int {
    
    case Assault_Occasioning_Actual_Bodily_Harm
    case Burglary
    case Kidnapping
    case Missing_Person
    case Obstructing_Officer_In_Execution_Of_Duty
    case Possession_Of_Control_Drug_Cannabis
    case Possession_Of_Firearm
    case Robbery
    case Stabbing
    case Theft
    case Theft_Of_Money
    case Wounding
    
}


 

