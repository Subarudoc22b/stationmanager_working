//
//  DivisionModel.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/6/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit

struct DivisionModel {
    let id: UUID
    var title: String
    var image: UIImage?
    var stationModels = [StationModel]()
//    var dayModels = [DayModel]() {
//        didSet {
//            //            // Called when a new value is assigned to dayModels
//            //            //            dayModels = dayModels.sorted(by: { (dayModel1, dayModel2) -> Bool in
//            //            //                dayModel1.title < dayModel2.title
//            //            //            })
//            //
//            //            //            dayModels = dayModels.sorted(by: { $0.title < $1.title })
//            //
//            dayModels = dayModels.sorted(by: <)
//        }
//    }
    
    
    
    init(title: String, image: UIImage? = nil, stationModels: [StationModel]? = nil)  {
        id = UUID()
        self.title = title
        self.image = image
        
        if let stationModels = stationModels {
            self.stationModels = stationModels
            
        }
        
//
//        if let dayModels = dayModels {
//            self.dayModels = dayModels
//
//
//        }
    }
    
}
