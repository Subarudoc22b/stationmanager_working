//
//  DivisionFunctions.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/6/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import Foundation
import MapKit

class DivisionFunctions {
    static func createDivision(divisionModel: DivisionModel) {
    }
    
    static func readDivision(completion: @escaping () -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            if Data.divisionModels.count == 0 {
                Data.divisionModels = DivisionData.createDivisionModelData()
//                Data.divisionModels.append(DivisionModel(title: "Northern Division"))
//                Data.divisionModels.append(DivisionModel(title: "Southern Division"))
                
            }
            
            DispatchQueue.main.async {
                completion()
            }
            
        }
        
    }
    
    static func readDivision(by id: UUID, completion: @escaping (DivisionModel?) -> ()) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            let division = Data.divisionModels.first(where: {$0.id == id})
            
            DispatchQueue.main.async {
                completion(division)
                
            }
            
        }
        
    }
    
    static func updateDivision(at index: Int, title: String, image: UIImage? = nil) {
      Data.divisionModels[index].title = title
        Data.divisionModels[index].image = image
        
    }
    
    static func deleteDivision(divisionModel: DivisionModel) {
        
    }
    
}

