//
//  Theme.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/7/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class Theme {
    static let mainFontName = "FugazOne-Regular"
    static let bodyFontName = "AvenirNext-Regular"
    static let bodyFontNameBold = "AvenirNext-Bold"
    static let bodyFontNameDemiBold = "AvenirNext-DemiBold"
    static let accent = UIColor(named: "Accent")
    static let backgroundColor = UIColor(named: "Background")
    static let tintColor = UIColor(named: "Tint")
}
