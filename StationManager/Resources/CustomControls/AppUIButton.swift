//
//  AppUIButton.swift
//  StationManager
//
//  Created by Shawn Coombs on 5/28/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class AppUIButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Theme.tintColor
        layer.cornerRadius = frame.height / 2
        setTitleColor(UIColor.white, for: .normal)
    }

}
