//
//  MorningReportModel.swift
//  StationManager
//
//  Created by Shawn Coombs on 6/23/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//
import UIKit
import MapKit

extension MorningReportModel  {
    
    class MorningReportModel: NSObject, MKAnnotation {
        var coordinate: CLLocationCoordinate2D
        
        init(coordinate: CLLocationCoordinate2D) {
            
            self.coordinate = coordinate
            
            
        }
        
    }
    
}
