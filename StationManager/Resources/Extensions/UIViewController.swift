//
//  UIViewController.swift
//  StationManager
//
//  Created by Shawn Coombs on 7/13/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /**
     Just returns the initial view controller on a storyboard
     */
    static func getInstance() -> UIViewController {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController()!
    }
}

