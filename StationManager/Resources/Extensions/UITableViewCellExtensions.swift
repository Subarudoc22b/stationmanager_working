//
//  UITableViewCellExtensions.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/17/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

extension UITableViewCell {
    /// Returns a string representation of this class
    class var identifier: String {
        return String(describing: self)
    }
}
