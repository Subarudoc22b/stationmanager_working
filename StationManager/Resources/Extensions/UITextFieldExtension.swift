//
//  UITextFieldExtension.swift
//  StationManager
//
//  Created by Shawn Coombs on 7/13/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

extension UITextField {
    var hasValue: Bool {
        guard text == "" else { return true }
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        imageView.image = #imageLiteral(resourceName: "warning")
        imageView.contentMode = .scaleAspectFit
        
        rightView = imageView
        rightViewMode = .unlessEditing
        
        return false
        
    }
    
}
