//
//  UIViewExtensions.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/7/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

extension UIView {
    func addShadowAndRoundedCorners() {
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.cornerRadius = 10
        
    }
    
}
