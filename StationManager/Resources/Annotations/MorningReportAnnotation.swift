//
//  MorningReportAnnotation.swift
//  StationManager
//
//  Created by Shawn Coombs on 6/28/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit

class MorningReportAnnotation: NSObject, MKAnnotation {

var id: String!
var title: String?
var subtitle: String?
var coordinate: CLLocationCoordinate2D
var reportType: ReportType?
var classType: String?
//var reports = [MorningReportModel]()

init( title: String, subtitle: String,  coordinate: CLLocationCoordinate2D, reportType: ReportType, classType: String ) {
    
    id = UUID().uuidString
    self.title = title
    self.subtitle = subtitle
    self.coordinate = coordinate
    self.reportType = reportType
    self.classType = classType
    
    super .init()
    
    
}

}
