//
//  AddMorningReportViewController.swift
//  StationManager
//
//  Created by Shawn Coombs on 5/28/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit

class AddMorningReportViewController: UIViewController {
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dayPickerView: UIPickerView!
    @IBOutlet weak var reportTitle: UITextField!
    @IBOutlet weak var reportDetails: UITextField!
    @IBOutlet weak var reportClass: UITextField!
    @IBOutlet weak var latitude: UITextField! 
    @IBOutlet weak var longitude: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var incidentNumber: UITextField!
    
    @IBOutlet var reportTypeButtons: [UIButton]!
    
    
    let reportPicker = UIPickerView()
    
    var doneSaving: ((Int, Int, MorningReportModel) -> ())?
    
    
    
    
    var divisionIndex: Int! // Needed for saving
    var stationIndex: Int!
    // var dayIndex: Int!
    var divisionModel: DivisionModel!
    var stationModels: StationModel! // Needed for showing days in picker view
    var morningReportModel: MorningReportModel!
    var coordinate: CLLocationCoordinate2D!
    
    var selectedDay: String?
    var selectedReport: String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createToolbar()
        createReportPicker()
        
        
        reportPicker.delegate = self
        reportPicker.dataSource = self
        reportTitle.inputView = reportPicker
        reportPicker.backgroundColor = .black
        //reportPicker.addShadowAndRoundedCorners()
        
        
        titleLabel.font = UIFont(name: Theme.mainFontName, size: 24)
        
        
        
        dayPickerView.delegate = self
        dayPickerView.dataSource = self
        dayPickerView.backgroundColor = .black
        dayPickerView.addShadowAndRoundedCorners()
        
        
    }
    
    func createdayPicker() {
        
        
        
    }
    
    func createReportPicker() {
        let reportPicker = UIPickerView()
        reportPicker.delegate = self
        reportPicker.dataSource = self
        reportTitle.inputView = reportPicker
        
        //  reportPicker.backgroundColor = .black
        //  reportPicker.tintColor = .white
        
        
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true)
        
    }
    
    @IBAction func reportTypeSelected(_ sender: UIButton) {
        reportTypeButtons.forEach({ $0.tintColor = Theme.accent })
        
        sender.tintColor = Theme.tintColor
    }
    
    
    @IBAction func save(_ sender: UIButton) {
        
        let reportType: ReportType = getSelectedReportType()
        
        let lat = Double(latitude.text!)
        let long = Double(longitude.text!)
        let dayIndex = dayPickerView.selectedRow(inComponent: 0)
       // let incident = Double(incidentNumber.text!)
        let morningReportModel = MorningReportModel(title: reportTitle.text!, subtitle: reportDetails.text ?? "", latitude:CLLocationDegrees(lat!), longitude: CLLocationDegrees(long!), reportType: getSelectedReportType() , classType: reportClass.text!, incidentNumber: incidentNumber.text!)
        
        MorningReportFunctions.createMorningReport(at: divisionIndex, at: stationIndex, for: dayIndex, using: morningReportModel)
        
        
        
        
        if let doneSaving = doneSaving {
            doneSaving(stationIndex, dayIndex, morningReportModel)
        }
        dismiss(animated: true)
    }
    
    func getSelectedReportType() -> ReportType {
        for (index, button) in reportTypeButtons.enumerated() {
            if button.tintColor == Theme.tintColor {
                return ReportType(rawValue: index) ?? .Burglary
                
            }
            
        }
        
        return .Burglary
    }
    
    func createToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        toolBar.barTintColor = .black
        toolBar.tintColor = .white
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AddMorningReportViewController.dismissKeyBoard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        reportTitle.inputAccessoryView = toolBar
        
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
    
}


extension AddMorningReportViewController: UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == dayPickerView {
            return stationModels.dayModels.count
        } else if pickerView == reportPicker {
            return pickerDataArray.count
        } else {
            return 0
        }
        
    }
    
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        if pickerView == dayPickerView {
    //            return stationModels!.dayModels[row].title.mediumDate()
    //        } else if pickerView == reportPicker {
    //            return pickerDataArray[row]
    //        } else {
    //            return ""
    //        }
    
    //  }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        //        let attributedString = NSAttributedString(string: pickerDataArray[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        //        return attributedString
        
        
        
        //        let font = UIFont.systemFont(ofSize: 72)
        //        let attributes: [NSAttributedString.Key: Any] = [
        //            .font: font,
        //            .foregroundColor: UIColor.red,
        //        ]
        //        let attributedQuote = NSAttributedString(string: pickerDataArray[row], attributes: attributes)
        //
        
        
        var attributedString: NSAttributedString!
        
        switch pickerView {
        case dayPickerView:
            attributedString = NSAttributedString(string: stationModels!.dayModels[row].title.mediumDate(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        case reportPicker:
            attributedString = NSAttributedString(string: pickerDataArray[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
            
        default:
            attributedString = nil
        }
        
        return attributedString
    }
    
    func pickerView(_: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedReport = pickerDataArray[row]
        reportTitle.text = selectedReport
        
    }
    
}





