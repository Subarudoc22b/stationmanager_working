//
//  StationViewController.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/13/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class StationViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var divisionId: UUID!
    var divisionTitle = ""
    var divisionModel: DivisionModel?
    
    var sectionHeaderHeight: CGFloat = 0.0
    
    
    
    fileprivate func updateTableViewWithDivisionData() {
        DivisionFunctions.readDivision(by: divisionId) { [weak self] (model) in
            guard let self = self else { return }
            self.divisionModel = model
            
            guard let model = model else { return }
            
            self.backgroundImageView.image = model.image
            
            self.tableView.reloadData()
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = divisionTitle
        
        tableView.dataSource = self
        tableView.delegate = self
        
        view.backgroundColor = Theme.backgroundColor
        
        updateTableViewWithDivisionData()
        
    }
        fileprivate func getDivisionIndex() -> Int! {
               
               return Data.divisionModels.firstIndex(where: { (divisionModels)  -> Bool in
                   divisionModels.id == divisionId
                   
               })
        
    }
    
}

extension StationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return divisionModel?.stationModels.count ?? 0
        
        //  return Data.divisionModels[section].stationModels.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stationModels = divisionModel?.stationModels[indexPath.row]
        // let stationModels = Data.divisionModels[indexPath.section].stationModels[indexPath.row]
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: StationTableViewCell.identifier) as! StationTableViewCell
        cell.setup(model: stationModels!)
        //  cell.setup(model: Data.divisionModels[indexPath.section].stationModels[indexPath.row])
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let stationModels = divisionModel?.stationModels[indexPath.row]
       
        let divisionIndex = getDivisionIndex()
        
        
        
        let storyboard = UIStoryboard(name: String(describing: MorningReportViewController.self), bundle: nil)
        let mrvc = storyboard.instantiateInitialViewController() as! MorningReportViewController
        mrvc.stationId = stationModels?.id
        mrvc.stationTitle = stationModels!.title!
     //   mrvc.stationModels = stationModels.self
        
        mrvc.divisionId = divisionModel?.id
      //  mrvc.divisionModel = divisionModel.self
        mrvc.divisionIndex = divisionIndex
        
        
        navigationController?.pushViewController(mrvc, animated: true)
        
        
    }
    
    
    
    
}


