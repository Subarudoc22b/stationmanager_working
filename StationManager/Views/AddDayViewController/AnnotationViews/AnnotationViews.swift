//
//  AnnotationViews.swift
//  StationManager
//
//  Created by Shawn Coombs on 7/6/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import Foundation
import MapKit

class MorningReportMarkerView: MKMarkerAnnotationView {
    
   
    override var annotation: MKAnnotation? {
        willSet {
            guard let report = newValue as? MorningReportModel else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            
            
            markerTintColor = report.markerTintColor
            
         //   glyphText = String((report.title?.first!)!)
            
            if let imageName = report.imageName {
                glyphImage = imageName
            } else {
                glyphImage = nil
            }
        }
    }
    
}

class MorningReportView: MKAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            guard let report = newValue as? MorningReportModel else {return}
            
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControl.State())
            rightCalloutAccessoryView = mapsButton
                  rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
           // if let imageName = report.imageName {
                // image = UIImage(named: imageName)
          //  } else {
          //      image = nil
         //   }
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = report.subtitle
            detailCalloutAccessoryView = detailLabel
        }
    }
    
}
