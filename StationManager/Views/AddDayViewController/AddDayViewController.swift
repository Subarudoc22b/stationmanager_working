//
//  AddDayViewController.swift
//  StationManager
//
//  Created by Shawn Coombs on 7/11/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit

class AddDayViewController: UIViewController {
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    
    
    // var doneSaving: (() -> ())?
    var doneSaving: ((Int, DayModel) -> ())?
    
    var divisionIndex: Int! // Needed for saving
    var stationIndex: Int!
    //  var dayIndex: Int!
    var divisionModel: DivisionModel!
    var dayModel: DayModel! // Needed for showing days in picker view
    var stationModels: StationModel!
    var morningReportModels: MorningReportModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.font = UIFont(name: Theme.mainFontName, size: 24)
        titleLabel.layer.shadowColor = UIColor.white.cgColor
        titleLabel.layer.shadowOffset = CGSize.zero
        titleLabel.layer.shadowRadius = 5
        
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true)
        
    }
    
    @IBAction func save(_ sender: UIButton) {
        // guard datePicker.hashValue, let newDay = datePicker.date else { return }
        if alreadyExists(datePicker.date) {
            let alert = UIAlertController(title: "Day Already Exists", message: "Choose another date", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel)
            alert.addAction(okAction)
            present(alert, animated: true)
            return
        }
        
        let dayModel = DayModel(title: datePicker.date, data: nil)
        DayFunctions.createDay(at: divisionIndex, at: stationIndex, using: dayModel)
        
        if let doneSaving = doneSaving {
            doneSaving(stationIndex, dayModel)
            
        }
        
        dismiss(animated: true)
    }
    
    func alreadyExists(_ date: Date) -> Bool {
        //        if tripModel.dayModels.contains(where: { (dayModel) -> Bool in
        //            return dayModel.title == date
        //        }) {
        //            return true
        //        }
        
        if stationModels.dayModels.contains(where: { $0.title.mediumDate() == date.mediumDate() }) {
            return true
        }
        
        return false
    }
    
    @IBAction func done(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}
