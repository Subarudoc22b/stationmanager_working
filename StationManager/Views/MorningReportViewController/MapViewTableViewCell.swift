//
//  MapViewTableViewCell.swift
//  StationManager
//
//  Created by Shawn Coombs on 8/25/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit




class MapViewTableViewCell: UITableViewCell, MKMapViewDelegate {
    
   
    @IBOutlet weak var mapView: MKMapView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mapView.delegate = self
       
     


        
}


    func setup(model: [MorningReportModel]) {
        
      

        mapView.register(MorningReportMarkerView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
    
        
        mapView.addAnnotations(model)
        
        
    }
    
    
    func setup(station: StationModel) {
    
      
//    mapView.register(MorningReportMarkerView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
       
        let center = station.coordinate
       
        mapView.setRegion(MKCoordinateRegion(center: center, span: span), animated: true)
        
        mapView.addAnnotation(station)
        
        }
    
    
}
