//
//  MorningReportViewController.swift
//  StationManager
//
//  Created by Shawn Coombs on 5/16/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit
import MapKit

class MorningReportViewController: UIViewController {
    
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    //    @IBOutlet weak var addButton: AppUIButton!
    
    
    
    
    var stationId: UUID!
    var stationTitle = ""
    var stationModels: StationModel?
    var sectionHeaderHeight: CGFloat = 0.0
    var sectionFooterHeaderHeight: CGFloat = 0.0
    var morningReportModel: MorningReportModel?
    
    var divisionId: UUID!
    var divisionModel: DivisionModel?
    var divisionIndex: Int!
   
    
    fileprivate func updateTableViewWithStationData() {
        StationFunctions.readStation(by: divisionIndex, by: stationId) { [weak self] (model) in
            guard let self = self else { return }
            self.stationModels = model

            


            //    guard let model = model else { return }

            //   self.backgroundImageView.image = model.image

            self.tableView.reloadData()
        }


    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        
        title = stationTitle
      
        
        tableView.dataSource = self
        tableView.delegate = self
        view.backgroundColor = Theme.backgroundColor
        
        
        updateTableViewWithStationData()
        
      //  tableView.rowHeight = UITableView.automaticDimension
      //  tableView.estimatedRowHeight = 200
        
        sectionHeaderHeight = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.identifier)?.contentView.bounds.height ?? 0
        sectionFooterHeaderHeight = tableView.dequeueReusableCell(withIdentifier: MapViewTableViewCell.identifier)?.contentView.bounds.height ?? 0
        }
     
    
    
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "toAddMorningReportSegue" {
//            let popup = segue.destination as! AddMorningReportViewController
//            popup.doneSaving = { [weak self] in
//                self!.tableView.reloadData()
         //   }

      //  }
    
  //  }

     @IBAction func addAction(_ sender: AppUIButton) {
        let alert = UIAlertController(title: "Which would you like to add?", message: nil, preferredStyle: .actionSheet)
        let dayAction = UIAlertAction(title: "Day", style: UIAlertAction.Style.default, handler: handleAddDay)
        let morningReportAction = UIAlertAction(title: "Report", style: UIAlertAction.Style.default, handler: handleAddReport)
//        { (action) in
//            print("Add new report")
//        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        
        if stationModels?.dayModels.count == 0 {
            morningReportAction.isEnabled = false
        }
        
        alert.addAction(dayAction)
        alert.addAction(morningReportAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = sender
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: -4, width: sender.bounds.width, height: sender.bounds.height)
        
        present(alert, animated: true)
        
    }
    
    fileprivate func getDivisionIndex() -> Int! {
        
        return Data.divisionModels.firstIndex(where: { (divisionModels)  -> Bool in
            divisionModels.id == divisionId
            
        })
        
    }
     
    fileprivate func getStationIndex() -> Int! {
        
        guard let divisionIndex = getDivisionIndex() else { return nil }
       
        
        return Data.divisionModels[divisionIndex].stationModels.firstIndex(where: { (stationModels) -> Bool in
            stationModels.id == stationId
            
        } )
        
    }
    
    func handleAddReport(action: UIAlertAction) {
        let vc = AddMorningReportViewController.getInstance() as! AddMorningReportViewController
        vc.morningReportModel = morningReportModel
        vc.divisionModel = divisionModel
        vc.stationModels = stationModels
        vc.stationIndex = getStationIndex()
        vc.divisionIndex = getDivisionIndex()
        
        vc.doneSaving = { [weak self] stationIndex, dayIndex, morningReportModel in
            guard let self = self else { return }
        self.divisionModel?.stationModels[stationIndex].dayModels[dayIndex].morningReportModels.append(morningReportModel)
            
            
            let row = (self.stationModels?.dayModels[dayIndex].morningReportModels.count)! - 1
            
            
            let indexPath = IndexPath(row: row, section: dayIndex)
            self.tableView.insertRows(at: [indexPath], with: .automatic)
            // self.tableView.reloadData()
        }
        
        present(vc, animated: true)
    }
    
    func handleAddDay(action: UIAlertAction) {
        let vc = AddDayViewController.getInstance() as! AddDayViewController
        vc.divisionModel = divisionModel
        vc.stationModels = stationModels
        vc.stationIndex = getStationIndex()
        vc.divisionIndex = getDivisionIndex()
        vc.doneSaving = { [weak self] stationIndex, dayModel in
            guard let self = self else { return }
            self.divisionModel?.stationModels[stationIndex].dayModels.append(dayModel)
            let indexArray = [self.stationModels?.dayModels.firstIndex(of: dayModel) ?? 0 ]
            self.tableView.insertSections(IndexSet(indexArray), with: UITableView.RowAnimation.automatic)
          
            
        }
        present(vc, animated: true)
    }
    
}


extension MorningReportViewController:  UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return stationModels?.dayModels.count ?? 0
        // return Data.stationModels.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sectionHeaderHeight
       // return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let dayModel = stationModels?.dayModels[section]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.identifier) as! HeaderTableViewCell
        cell.setup(model: dayModel!)
        return cell.contentView
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return stationModels?.dayModels[section].morningReportModels.count ?? 0
        
    }
    
    func tabbleView(_tableview: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modelReport = stationModels?.dayModels[indexPath.section].morningReportModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: MorningReportTableViewCell.identifier) as! MorningReportTableViewCell
        cell.setup(modelReport: modelReport!)
        return cell
        
        // return UITableViewCell(style: .default, reuseIdentifier: "cell")
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return sectionFooterHeaderHeight
        // return 300
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let model = stationModels?.dayModels[section].morningReportModels
        let station = stationModels
        let cell = tableView.dequeueReusableCell(withIdentifier: MapViewTableViewCell.identifier) as! MapViewTableViewCell
        cell.setup(model: model!)
        cell.setup(station: station!)
        
        return cell.contentView
        //  return UITableViewCell(style: .default, reuseIdentifier: "MapViewTableViewCell")
        
        
    }
    
}



