//
//  HeaderTableViewCell.swift
//  StationManager
//
//  Created by Shawn Coombs on 5/16/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
      //  titleLabel.font = UIFont(name: Theme.bodyFontNameBold, size: 17)
      //  subtitleLabel.font = UIFont(name: Theme.bodyFontName, size: 15)
        
    }

    func setup(model: DayModel) {
        titleLabel.text = model.title.mediumDate()
        
    }
}
