//
//  MorningReportTableViewCell.swift
//  StationManager
//
//  Created by Shawn Coombs on 5/16/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class MorningReportTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var subTitleLabel: UILabel?
    @IBOutlet weak var reportClassLabel: UILabel!
    @IBOutlet weak var reportTypeImage: UIImageView!
    @IBOutlet weak var incidentNumberLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        cardView.addShadowAndRoundedCorners()
       // titleLabel?.font = UIFont(name: Theme.bodyFontNameDemiBold, size: 17)
      //  subTitleLabel!.font = UIFont(name: Theme.bodyFontName, size: 17)
        cardView.backgroundColor = Theme.accent
    }
    
    func setup(modelReport: MorningReportModel) {
        reportTypeImage.image = getReportTypeImage(type: modelReport.reportType!)
        titleLabel!.text = modelReport.title
        subTitleLabel!.text = modelReport.subtitle
        reportClassLabel.text = modelReport.classType
        incidentNumberLabel.text = modelReport.incidentNumber 
    }

 func getReportTypeImage(type: ReportType) -> UIImage? {
    
    switch type {
    case .Assault_Occasioning_Actual_Bodily_Harm:
        return #imageLiteral(resourceName: "Assault_Occasioning_Actual_Bodily_Harm")
    case .Burglary:
        return #imageLiteral(resourceName: "Burglary")
    case .Kidnapping:
        return #imageLiteral(resourceName: "Kidnapping")
    case .Missing_Person:
        return #imageLiteral(resourceName: "Missing_Person")
    case .Obstructing_Officer_In_Execution_Of_Duty:
        return #imageLiteral(resourceName: "Obstructing_Officer_In_Execution_Of_Duty")
    case .Possession_Of_Control_Drug_Cannabis:
        return #imageLiteral(resourceName: "Possession_Of_Control_Drug_Cannabis")
    case .Possession_Of_Firearm:
        return #imageLiteral(resourceName: "Possession_Of_Firearm")
    case .Robbery:
        return #imageLiteral(resourceName: "Robbery")
    case .Stabbing:
        return #imageLiteral(resourceName: "Stabbing")
    case .Theft_Of_Money:
        return #imageLiteral(resourceName: "Theft_Of_Money")
    case .Theft:
        return #imageLiteral(resourceName: "Theft")
    case .Wounding:
        return #imageLiteral(resourceName: "Wounding")
        
    }
  }
}
