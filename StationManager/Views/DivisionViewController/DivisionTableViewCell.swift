//
//  DivisionTableViewCell.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/7/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class DivisionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var divisionImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cardView.addShadowAndRoundedCorners()
        titleLabel.font = UIFont(name: Theme.mainFontName, size: 32)
        cardView.backgroundColor = Theme.accent
        divisionImageView.layer.cornerRadius = cardView.layer.cornerRadius
        
    }
    
    func setup(divisionModel: DivisionModel) {
        titleLabel.text = divisionModel.title
        
        if let divisionImage = divisionModel.image {
            divisionImageView.alpha = 0.3
            divisionImageView.image = divisionImage
            
            UIView.animate(withDuration: 1) {
                self.divisionImageView.alpha = 1
            }
        }
    }
}
