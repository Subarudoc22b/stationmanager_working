//
//  DivisionViewController.swift
//  StationManager
//
//  Created by Shawn Coombs on 4/6/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class DivisionViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.dataSource = self
        tableView.delegate = self 
        
        DivisionFunctions.readDivision(completion: {[weak self] in
        
        
            self?.tableView.reloadData()
            
            
        })
        
        view.backgroundColor = Theme.backgroundColor
     
        
    
    
    }

}


extension DivisionViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.divisionModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DivisionTableViewCell.identifier) as! DivisionTableViewCell
        
        cell.setup(divisionModel: Data.divisionModels[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    //Segue function from Tableview cell to New VC
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let division = Data.divisionModels[indexPath.row]
       

        let storyboard = UIStoryboard(name: String(describing: StationViewController.self), bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! StationViewController
        vc.divisionId = division.id
        vc.divisionTitle = division.title
        navigationController?.pushViewController(vc, animated: true)
        
        
        }
    
    
}
 
