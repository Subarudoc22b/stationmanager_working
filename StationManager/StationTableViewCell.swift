//
//  StationTableViewCell.swift
//  StationManager
//
//  Created by Shawn Coombs on 8/25/19.
//  Copyright © 2019 Shawn Coombs. All rights reserved.
//

import UIKit

class StationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardView.addShadowAndRoundedCorners()
        titleLabel.font = UIFont(name: Theme.bodyFontNameDemiBold, size: 22)
        
        
    }
    
    func setup(model: StationModel) {
        titleLabel.text = model.title
    
        
    }
    
}

